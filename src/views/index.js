import Vue from 'vue';
import CesiumControl  from "./map_cs/CesiumControl.vue"

const components = [
    CesiumControl,
];

function install(Vue) {
    components.map(component => {
        Vue.component(component.name, component);
    });
}

if (window.Vue) {
    install(window.Vue);
} else {
    install(Vue);
}
//导出组件库
export default install;