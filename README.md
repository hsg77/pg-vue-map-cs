# pg-vue-map-cs-master
# pg-vue-map-cs

> A Vue.js project
> #### 介绍
基于vue和cesium封装的地图组件

# 创建 vue 项目
npm install -g @vue/cli-init     //下面命令报错后先执行此行
vue init webpack-simple pg-vue-map-cs-master

#### 软件架构
软件架构说明

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# 打包 为 dist/pg-vue-map-cs.min.js
npm run build

# 打包 为pg-vue-map-cs-1.0.0.tgz文件
npm pack

# tgz文件在应用vue项目占点下载安装
npm install d:\vue_plugin\pg-vue-map-cs-1.0.0.tgz

# 应用项目中引用
import CesiumControl  from 'CesiumControl'
import 'CesiumControl/lib/css/...css'
Vue.use(CesiumControl);

# 如果可以用了，就直接上传到npm官网库
npm login  
# 需要填写账号，密码，邮箱
Username:
Password:
Email:
# 登录成功后，会返回
Logged in as [用户名] on [npm库地址]

# 上传npm包  到npm官网库
npm publish

# 每次更新之前要更改package.json文件里的版本号 或者
npm run build  
npm version patch

# 下载测试
npm install loading
npm install pg-vue-map-cs

# command login npm error handler:
npm config set registry https://registry.npmjs.org/

# check url 
curl https://registry.npmjs.org/

# clean npm cache
npm cache clean --force

# relogin npm 
npm adduser

#  query npm logined ?
npm who am i

# 是npm accout email check  E403
E403 Forbidden - PUT https://registry.npmjs.org/pg-vue-map-cs - Forbidden

# ---the---end---

